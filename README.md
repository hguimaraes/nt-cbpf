## NT-CBPF

 The "NT" (Technical paper in portuguese) is a special type of publication in the Brazilian Center for Physics Research (CBPF). It's intended to publish original papers about technological research and theoretical or computational approaches in others fields of knowledge.

### How to use

* git clone https://github.com/Hguimaraes/NT-CBPF.git

Copy the nt.sty and the cbpf.cls to the folder of your project, import the nt.sty into your Tex file and be happy. You also can use the "templateNT.tex" to start a new NT.

### Acknowledgment

* André A. Persechino
